import re
from datetime import datetime, time
from math import ceil


class FileParser:
    def get_all_items_from_key(self, data: str, key: str):
        data = data.split('  ')
        expression = ''
        for char in key:
            expression += f'[{char.upper()}]'
        expression += '\w{2,}'
        toreturn = []
        for line in data:
            if re.findall(f'{expression}', line):
                toreturn.append(str(line))
        return toreturn


class Validity:
    def __init__(self, originalline: str):
        twodigits = re.findall(r'\d{2}', originalline)
        self.day = int(twodigits[0])
        self.month = int(twodigits[1])
        self.year = int(f'{twodigits[2]}{twodigits[3]}')
        self.hour = int(twodigits[4])
        self.minutes = int(twodigits[5])

    @property
    def fulldate(self):
        return datetime(self.year, self.month, self.day, self.hour, self.minutes)


class Area:
    def __init__(self, originalline: str, uupwef, uuptil, sflcalculation, usertextoutput, shortareaname):
        self.uupwef = uupwef
        self.uuptil = uuptil
        title = re.findall(r'\w{4,}', originalline)
        self.rsa = title[0]
        levels = re.findall(r'\t\d{3}', originalline)
        self.mnmfl = int(levels[0])
        self.maxfl = int(levels[1])
        hours = re.findall(r'\d{2}[:]\d{2}', originalline)
        wefhour = int(hours[0][:2])
        wefminutes = int(hours[0][-2:])
        self.wef = time(wefhour, wefminutes)
        unthour = int(hours[1][:2])
        untminutes = int(hours[1][-2:])
        self.unt = time(unthour, untminutes)
        self.sflcalculation = sflcalculation
        self.usertextoutput = usertextoutput
        self.shortareaname = shortareaname

    def __str__(self):
        if self.usertextoutput:
            return f'{self.name}:{self.startdate}:{self.enddate}:{self.weekdays}:{self.starttime}:{self.endtime}:{self.lower}:{self.upper}:{self.usertext}'
        else:
            return f'{self.name}:{self.startdate}:{self.enddate}:{self.weekdays}:{self.starttime}:{self.endtime}:{self.lower}:{self.upper}'

    @property
    def name(self):
        if self.shortareaname:
            return self.rsa[2:]
        else:
            return self.rsa

    @property
    def startdate(self):
        return self.uupwef.strftime("%y%m%d")

    @property
    def starttime(self):
        return self.wef.strftime("%H%M")

    @property
    def enddate(self):
        if self.unt > self.uuptil.time():
            return self.uupwef.strftime("%y%m%d")
        else:
            return self.uuptil.strftime("%y%m%d")

    @property
    def endtime(self):
        return self.unt.strftime("%H%M")

    @property
    def weekdays(self):
        return 0

    @property
    def lower(self):
        return self.mnmfl * 100

    @property
    def upper(self):
        return self.maxfl * 100

    @property
    def usertext(self):
        if self.sflcalculation:
            if self.maxfl * 100 + 1000 > 4500:
                if self.maxfl > 640:
                    if(self.mnmfl > 10):
                        return f'SFL{self.calculate_safe_level()} MAX'
                    else:
                        return f'SFLUNL'
                else:
                    return f'SFL{self.calculate_safe_level()}'
            else:
                if(self.mnmfl > 10):
                        return f'SFA{self.calculate_safe_level()*100} MAX'
                return f'SFA{self.calculate_safe_level()*100}'
        else:
            return f'{self.mnmfl}-{self.maxfl}'

    def calculate_safe_level(self):
        if self.maxfl > 640:
            if self.mnmfl > 10
                return self.mnmfl - 10
            else
                return self.maxfl
        flightlevel = self.maxfl
        if self.maxfl > 35:
            flightlevel = ceil(self.maxfl / 10) * 10
        flightlevel += 10
        if flightlevel > 410:
            if flightlevel % 20 == 0:
                flightlevel += 10
            if flightlevel - self.maxfl <= 20:
                flightlevel += 20
        return flightlevel
