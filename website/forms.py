from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, BooleanField
from wtforms.validators import DataRequired, Length, ValidationError, Length


class RegistrationForm(FlaskForm):
    rawdata = StringField('Raw Data', validators=[DataRequired()])
    countryicaocode = StringField(
        'Country ICAO Code', validators=[DataRequired(), Length(min=2, max=2)])
    sflcalculation = BooleanField('Safe Level Calculation', default=True)
    usertextoutput = BooleanField('User Text Output', default=True)
    shortareaname = BooleanField('Short RSA Area Name', default=True)
    submit = SubmitField('Send')
