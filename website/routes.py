import re
from flask import render_template, request, flash

from website import app

from .entities import Area, FileParser, Validity
from .forms import RegistrationForm


@app.route('/parser', methods=['GET', 'POST'])
def parse_data():
    form = RegistrationForm()
    if form.validate_on_submit():
        try:
            uupfulldate = re.findall(
                r'\d{2}\/\d{2}\/\d{4}\s\d{2}[:]\d{2}', form.rawdata.data)
            uupwef = Validity(uupfulldate[0])
            uuptil = Validity(uupfulldate[1])
            results = FileParser().get_all_items_from_key(
                form.rawdata.data, form.countryicaocode.data)
            if len(results) == 0:
                raise UserWarning('No info found for your Country')
            output = []
            for result in results:
                area = Area(result, uupwef.fulldate, uuptil.fulldate,
                            form.sflcalculation.data, form.usertextoutput.data,
                            form.shortareaname.data)
                output.append(str(area))
            return render_template('results.html', title='Results', posts=output)
        except IndexError:
            flash('Data is not in the correct format', 'danger')
        except UserWarning as e:
            flash(e, 'danger')
    return render_template('parser.html', title='AUP/UUP Parser', form=form)


@app.route('/')
def home():
    return render_template('home.html')
